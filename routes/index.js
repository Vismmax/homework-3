// import loginRoutes from "./loginRoutes";
import gameRoutes from "./gameRoutes";
import appRoutes from "./appRoutes";

export default app => {
  // app.use("/login", loginRoutes);
  app.use("/game", gameRoutes);
  app.use("/app", appRoutes);
};
