import { Router } from "express";
import path from "path";
import { HTML_FILES_PATH } from "../config";
import getTextById from '../helpers/dataApi';

const router = Router();

router
  // .get("/", (req, res) => {
  //   const page = path.join(HTML_FILES_PATH, "game.html");
  //   res.sendFile(page);
  // })
  .get('/texts/:id', (req, res) => {
    console.log(req.params.id);
    const text = getTextById(req.params.id);
    res.send({ text });
  });

export default router;
