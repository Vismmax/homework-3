import hat from "hat";
import * as config from "./config";
import { texts } from "../data";
import Room from "./Room";
import Rooms from "./Rooms";
import Users from "./Users";

const users = new Users();
const rooms = new Rooms();

const getCurrentUsername = (socket) => socket.handshake.query.userName;
const getCurrentRoomId = (socket) =>
  Object.keys(socket.rooms).find((id) => rooms.getRoomById(id));

export default (io) => {
  io.on("connection", (socket) => {
    // let username = "";
    // let currentUser = {};
    socket.emit("UPDATE_ROOMS", rooms.getRooms());

    socket.on("LOGIN_USER", (name) => {
      if (!name) return;
      if (users.getUserByName(name)) {
        socket.emit("ERROR_LOGIN", "User Exist");
        return;
      }
      socket.emit("USER_LOGIN", name);
      socket.emit("UPDATE_ROOMS", rooms.getRooms());
      users.addUser(name);
      // currentUser = users.addUser(name);
      // username = name;
    });

    socket.on("CREATE_ROOM", ({ name, userName }) => {
      if (rooms.getRoomByName(name)) {
        socket.emit("ERROR_CREATE_ROOM", "Exist room");
        return;
      }
      const room = rooms.addRoom(name);
      socket.join(room.id, () => {
        const user = users.getUserByName(userName);
        rooms.addUserToRoom(room.id, user);
        socket.emit("JOIN_ROOM_DONE", rooms.getRoomById(room.id));
        io.emit("UPDATE_ROOMS", rooms.getRooms());
      });
    });

    socket.on("JOIN_ROOM", ({ id, userName }) => {
      socket.join(id, () => {
        const user = users.getUserByName(userName);
        const room = rooms.addUserToRoom(id, user);
        socket.emit("JOIN_ROOM_DONE", room);
        io.emit("UPDATE_ROOMS", rooms.getRooms());
        io.to(id).emit("UPDATE_ACTIVE_ROOM", room);
      });
    });

    socket.on("LEAVE_ROOM", ({ id, userName }) => {
      socket.leave(id);
      const user = users.getUserByName(userName);
      const room = rooms.deleteUserFromRoom(id, user);
      socket.emit("LEAVE_ROOM_DONE", room);
      io.emit("UPDATE_ROOMS", rooms.getRooms());
      io.to(id).emit("UPDATE_ACTIVE_ROOM", room);
    });

    socket.on("TOGGLE_READY", ({ userName }) => {
      const user = users.getUserByName(userName);
      const roomId = getCurrentRoomId(socket);
      const room = rooms.toggleReadyUser(roomId, user);
      io.to(roomId).emit("UPDATE_ACTIVE_ROOM", room);
      if (room.isReady()) startGame(roomId);
    });

    const startGame = (roomId) => {
      setTimer(roomId, runGame);
      const room = rooms.getRoomById(roomId);
      const textId = Math.floor(Math.random() * texts.length);
      io.to(roomId).emit("UPDATE_ACTIVE_ROOM", room);
      io.to(roomId).emit("UPDATE_TEXT_GAME", textId);
    };

    const setTimer = (roomId, cb) => {
      let time = config.SECONDS_TIMER_BEFORE_START_GAME;
      io.to(roomId).emit("UPDATE_TIMER", time--);
      const interval = setInterval(() => {
        io.to(roomId).emit("UPDATE_TIMER", time--);
        if (time < 0) {
          clearInterval(interval);
          cb(roomId);
        }
      }, 1000);
    };

    const runGame = (roomId) => {
      let gameTime = config.SECONDS_FOR_GAME;
      io.to(roomId).emit("UPDATE_GAME_TIMER", gameTime--);
      const interval = setInterval(() => {
        io.to(roomId).emit("UPDATE_GAME_TIMER", gameTime--);
        if (gameTime < 0) {
          clearInterval(interval);
          // cb(roomId);
        }
      }, 1000);
      console.log("RUN_GAME");
      io.to(roomId).emit("RUN_GAME");
    };
  });
};
