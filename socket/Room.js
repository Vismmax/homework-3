import hat from "hat";
import Users from "./Users";

class Room {
  constructor(name) {
    this.id = hat();
    this.name = name;
    this.users = [];
  }

  get countUser() {
    return this.users.length;
  }

  addUser(user) {
    this.users.push(user);
    return user;
  }

  deleteUser(user) {
    const id = this.users.findIndex((e) => e.id === user.id);
    this.users.splice(id, 1);
    // if (id === 0 && this.users.length === 1) this.users = [];
    return user;
  }

  toggleReadyUser(user) {
    this.users = this.users.map((us) => {
      if (us.id === user.id) {
        us.isReady = !us.isReady;
      }
      return us;
    });
  }

  isReady() {
    return this.users.every((e) => e.isReady);
  }
}

export default Room;
