import User from "./User";

// class Users {
//   constructor() {
//     this.users = new Map()
//   }
//
//   addUser(name) {
//     const user = new User(name);
//     this.users.set(user.id, user);
//     return user;
//   }
//
//   getUserById(id) {
//     return this.users.get(id);
//   }
//
//   getUserByName(name) {
//     for (let value of this.users.values()) {
//       if (value.name === name) {
//         return value;
//       }
//     }
//     return false;
//   }
// }

class Users {
  constructor() {
    this.users = [];
  }

  addUser(name) {
    const user = new User(name);
    this.users.push(user);
    return user;
  }

  getUserById(id) {
    return this.users.find((e) => e.id === id);
  }

  getUserByName(name) {
    return this.users.find((e) => e.name === name);
  }

  deleteUserById(id) {
    const idx = this.users.findIndex((e) => e.id === id);
    this.users.splice(idx, 1);
    // if (idx === 0 && this.users.length === 1) this.users = [];
    return id;
  }
}

export default Users;
