import Room from "./Room";

// class Rooms {
//   constructor(props) {
//     this.rooms = new Map();
//   }
//
//   addRoom(name) {
//     const room = new Room(name);
//     this.rooms.set(room.id, room);
//     return room;
//   }
//
//   getRooms() {
//     const arrayRooms = [];
//     for (let { id, name, countUser } of this.rooms.values()) {
//       arrayRooms.push({
//         id,
//         name,
//         countUser,
//       });
//     }
//     return arrayRooms;
//   }
//
//   getRoomByName(name) {
//     for (let value of this.rooms.values()) {
//       if (value.name === name) {
//         return value;
//       }
//     }
//     return false;
//   }
//
//   deleteRoomById(id) {
//     this.rooms.delete(id);
//   }
//
//   addUserToRoom(roomId, user) {
//     const room = this.rooms.get(roomId);
//     room.addUser(user);
//   }
//
//   deleteUserFromRoom(roomId, user) {
//     const room = this.rooms.get(roomId);
//     room.deleteUser(user);
//   }
// }

class Rooms {
  constructor(props) {
    this.rooms = [];
  }

  addRoom(name) {
    const room = new Room(name);
    this.rooms.push(room);
    return room;
  }

  getRooms() {
    return this.rooms.map(({ id, name, countUser }) => ({
      id,
      name,
      countUser,
    }));
  }

  getRoomById(id) {
    return this.rooms.find((e) => e.id === id);
  }

  getRoomByName(name) {
    return this.rooms.find((e) => e.name === name);
  }

  deleteRoomById(id) {
    const idx = this.rooms.findIndex((e) => e.id === id);
    this.rooms.splice(idx, 1);
    // if (idx === 0 && this.rooms.length === 1) this.rooms = [];
    return id;
  }

  addUserToRoom(roomId, user) {
    this.rooms = this.rooms.map((room) => {
      if (room.id === roomId) {
        room.addUser(user);
      }
      return room;
    })
    return this.getRoomById(roomId);
  }

  deleteUserFromRoom(roomId, user) {
    this.rooms = this.rooms.map((room) => {
      if (room.id === roomId) {
        room.deleteUser(user);
      }
      return room;
    })
    if (this.getRoomById(roomId).countUser === 0) {
      this.deleteRoomById(roomId);
    }
    return this.getRoomById(roomId);
  }

  toggleReadyUser(roomId, user) {
    this.rooms = this.rooms.map((room) => {
      if (room.id === roomId) {
        room.toggleReadyUser(user);
      }
      return room;
    })
    return this.getRoomById(roomId);
  }
}

export default Rooms;
