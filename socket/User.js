import hat from "hat";

class User {
  constructor(name) {
    this.id = hat();
    this.name= name;
    this.isReady = false;
    this.completed = 0;
  }
}

export default User;