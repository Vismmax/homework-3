export type TypeUser = {
  id: string;
  name: string;
  isReady: boolean;
  completed: number;
};

export type TypeRoom = {
  id: string;
  name: string;
  countUsers: number;
  users: TypeUser[];
  textId?: string;
  text?: string;
  timer?: number;
  timeGame?: number;
};