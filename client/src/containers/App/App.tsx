import React, { useEffect, useState } from 'react';
import "./App.css";
import Login from "../../components/Login/Login";
import io from "socket.io-client";
import { TypeRoom as TypeRoomCard } from "../../components/CardRoom/CardRoom";
import ListRooms from "../../components/ListRooms/ListRooms";
import Room from "../../components/Room/Room";
import { TypeRoom } from "../../models/types";
import { callApi } from "../../helpers/webApi";

const socket = io("");

function App(): JSX.Element {
  const [userName, setUserName] = useState(
    sessionStorage.getItem("username") ?? ""
  );
  const [rooms, setRooms] = useState([] as TypeRoomCard[]);
  const [activeRoom, setActiveRoom] = useState(null as TypeRoom | null);
  const [timer, setTimer] = useState(null as number | null);
  const [gameTimer, setGameTimer] = useState(0 as number);
  const [text, setText] = useState("");

  const loginUser = (name: string) => {
    socket.emit("LOGIN_USER", name);
  };

  const createRoom = (name: string) => {
    socket.emit("CREATE_ROOM", { name, userName });
  };

  const joinRoom = (id: string) => {
    socket.emit("JOIN_ROOM", { id, userName });
  };

  const leaveRoom = (id: string) => {
    socket.emit("LEAVE_ROOM", { id, userName });
  };

  const toggleReadyUser = () => {
    socket.emit("TOGGLE_READY", { userName });
  };

  useEffect(() => {
    socket.on("USER_LOGIN", (name: string) => {
      sessionStorage.setItem("username", name);
      setUserName(name);
    });

    socket.on("ERROR_LOGIN", (error: string) => {
      sessionStorage.removeItem("username");
      alert(error);
    });

    socket.on("UPDATE_ROOMS", (arrayRooms: TypeRoomCard[]) =>
      setRooms(arrayRooms)
    );

    socket.on("ERROR_CREATE_ROOM", (error: string) => {
      alert(error);
    });

    socket.on("JOIN_ROOM_DONE", (room: TypeRoom) => {
      setActiveRoom(room);
    });

    socket.on("LEAVE_ROOM_DONE", (room: TypeRoom) => {
      setActiveRoom(null);
    });

    socket.on("UPDATE_ACTIVE_ROOM", (room: TypeRoom) => {
      setActiveRoom(room);
    });

    socket.on("UPDATE_TIMER", (timer: number) => {
      setTimer(timer);
    });

    socket.on("UPDATE_GAME_TIMER", (timer: number) => {
      setGameTimer(timer);
    });

    socket.on("UPDATE_TEXT_GAME", (textId: number) => {
      callApi(`/game/texts/${textId}`, "GET")
        .then((data) => {
          console.log(data.text);
          setText(data.text);
        });
    });

    socket.on("RUN_GAME", (timer: number) => {
      console.log("RUN_GAME");
    });

    document.addEventListener('keypress', (ev)=>{
      console.log(ev.key);
    });
  }, []);

  return (
    <div className="App">
      {!userName && <Login login={loginUser} />}
      {userName && (
        <>
          {!activeRoom && (
            <ListRooms
              rooms={rooms}
              createRoom={createRoom}
              joinRoom={joinRoom}
            />
          )}
          {activeRoom && (
            <Room
              room={activeRoom}
              currentUserName={userName}
              timer={timer}
              gameTimer={gameTimer}
              text={text}
              leaveRoom={leaveRoom}
              toggleReadyUser={toggleReadyUser}
            />
          )}
        </>
      )}
    </div>
  );
}

export default App;
