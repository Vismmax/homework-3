import React from "react";
import "./ListRooms.css";
import CardRoom, { TypeRoom } from "../CardRoom/CardRoom";

type Props = {
  rooms: TypeRoom[];
  createRoom: (name: string) => void;
  joinRoom: (id: string) => void;
};

function ListRooms({ rooms, createRoom, joinRoom }: Props): JSX.Element {

  const newRoom = () => {
    const nameRoom = prompt("Name new room");
    if (nameRoom) {
      createRoom(nameRoom);
    }
  };

  const join = (id: string) => joinRoom(id);

  const cards = rooms.map((room: TypeRoom) => <CardRoom key={room.id} room={room} joinRoom={join} />);

  return (
    <div id="rooms-page" className="rooms-page full-screen">
      <h1 className="rooms-page-title">Join Room Or Create New</h1>
      <button className="button-create-room" onClick={newRoom}>
        Create Room
      </button>
      <div id="cards-room" className="cards-room">
        {cards}
      </div>
    </div>
  );
}

export default ListRooms;
