import React from "react";
import "./UserInfo.css";
import { TypeUser } from "../../models/types";

type Props = {
  user: TypeUser;
  current: boolean;
};

function UserInfo({ user, current }: Props): JSX.Element {
  const { name, isReady } = user;
  return (
    <div className="user-info">
      <div className="user-title">
        <span className={`user-status ${isReady ? "ready" : ""}`} />
        <span className={`user-name ${current ? "current-user" : ""}`}>
          {name}
          {current ? " (you)" : ""}
        </span>
      </div>
      <div className="user-progress">
        <div className="user-progress-bar" />
      </div>
    </div>
  );
}

export default UserInfo;
