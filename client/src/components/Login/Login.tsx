import React, { useState } from "react";
import "./Login.css";

type Props = {
  // login: (name: string) => void;
  login: any;
};

function Login({ login }: Props): JSX.Element {
  const [name, setName] = useState('');

  const onChangeHandler = (ev: React.ChangeEvent<HTMLInputElement>): void => {
    setName(ev.target.value);
  };

  const onSubmitHandler = (ev: React.FormEvent<HTMLFormElement>): void => {
    console.log('onSubmitHandler', name);
    ev.preventDefault();
    if (!name) {
      return;
    }
    login(name);
  };

  return (
    <div className="full-screen flex-centered">
      <form className="flex" onSubmit={onSubmitHandler}>
        <div className="username-input-container">
          <input
            autoFocus
            placeholder="username"
            type="text"
            value={name}
            onChange={onChangeHandler}
          />
        </div>
        <button
          className="submit-button no-select"
          type="submit"
          // disabled={!name}
        >
          submit
        </button>
      </form>
    </div>
  );
}

export default Login;
