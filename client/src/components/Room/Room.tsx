import React from "react";
import "./Room.css";
import UserInfo from "../UserInfo/UserInfo";
import { TypeUser, TypeRoom } from "../../models/types";

type Props = {
  room: TypeRoom;
  currentUserName: string;
  timer: number | null;
  gameTimer: number | null;
  text: string;
  leaveRoom: (id: string) => void;
  toggleReadyUser: () => void;
};

function Room({
  room,
  currentUserName,
  timer,
  gameTimer,
  text,
  leaveRoom,
  toggleReadyUser,
}: Props): JSX.Element {
  const currentUser = room.users.find(
    (e) => e.name === currentUserName
  ) as TypeUser;

  const leave = () => {
    leaveRoom(room.id);
  };

  const users = room.users.map((user) => (
    <UserInfo
      key={user.id}
      user={user}
      current={user.name === currentUserName}
    />
  ));

  return (
    <div className="room-page">
      <div className="room-aside">
        <h1>{room.name}</h1>
        <button className="button-back" onClick={leave}>
          &lt; Back To Rooms
        </button>
        <div className="users-info">{users}</div>
      </div>
      <div className="room-main">
        {!timer && !text && (
          <button className="button-user-status" onClick={toggleReadyUser}>
            {currentUser.isReady ? "Not ready" : "Ready"}
          </button>
        )}
        <div className={`room-counter ${timer?'':'display-none'}`}>{timer}</div>
        <div className={`game-text ${!timer && text?'':'display-none'}`}>{text}</div>
        <div className={`game-timer ${gameTimer?'':'display-none'}`}>{gameTimer} seconds left</div>
      </div>
    </div>
  );
}

export default Room;
