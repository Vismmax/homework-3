import React, { useState } from 'react';
import "./GameTimer.css";
import { TypeUser } from "../../models/types";

type Props = {
  time: number;
  endGameTimer: ()=>(void);
};

function GameTimer({ time, endGameTimer }: Props): JSX.Element {
  const [ timer, setTimer ] = useState(time);
  return (
    <div className="game-timer">
      {timer}
    </div>
  );
}

export default GameTimer;
