import React from "react";
import "./CardRoom.css";

export type TypeRoom = {
  id: string;
  name: string;
  countUser: number;
};

type Props = {
  room: TypeRoom;
  joinRoom: (id: string) => void;
};

function CardRoom({ room, joinRoom }: Props): JSX.Element {
  const { name, countUser } = room;
  const join = () => joinRoom(room.id);

  return (
    <div className="card-room">
      <div className="card-users">{countUser} users connected</div>
      <h2 className="card-title">{name}</h2>
      <button className="card-btn-join" onClick={join}>
        Join
      </button>
    </div>
  );
}

export default CardRoom;
