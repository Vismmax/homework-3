const API_URL = '';

async function callApi(endpoint: string, method: string) {
  const url = API_URL + endpoint;
  const options = {
    method,
  };

  return fetch(url, options)
    .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
    // .then((result) => JSON.parse(atob(result.content)))
    .catch((error) => {
      throw error;
    });
}

export { callApi };
