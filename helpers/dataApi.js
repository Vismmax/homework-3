import { texts } from "../data";

const getTextById = (id) => {
  return texts[id];
};

export default getTextById;
